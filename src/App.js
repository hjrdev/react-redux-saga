import React from 'react';

import TodoList from './pages/TodoList'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>TODO: REACT & REDUX SAGA</h1>
      </header>
      <TodoList />
    </div>
  );
}

export default App;
