import { takeLatest, put, delay, call } from 'redux-saga/effects'

function* sagaAddTodo(action) {
  yield call(delay, 4000)

  yield put({ type: 'ADD_TODO', 
  payload: { text: action.payload.text } });
}

export default function* root() {
  yield [
    takeLatest('SAGA_ADD_TODO', sagaAddTodo)
  ]
}